from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from .models import Jadwals
from .forms import FormJadwal

# Create your views here.
response = {'author' : 'Nabilah Adani Nurulizzah'}
def pembuka(request):
    return render(request, 'Page1.html')

def tentangsaya(request):
    return render(request, 'Page2.html')

def selengkapnya(request):
    return render(request, 'Page3.html')

def registrasi(request):
	return render(request, 'Page4.html')

def buatjadwal(request):
	jwl = Jadwals.objects.all()
	form = FormJadwal(request.POST)
	if request.method == 'POST':
		if 'DeleteAll' in request.POST :
			Jadwals.objects.all().delete()
			context = {
				'form' : form,
				'jwl' : jwl
			}
			return render(request, 'Jadwal.html', context)	
		response['nama'] = request.POST['nama']
		response['tipe'] = request.POST['tipe']
		response['tanggal'] = request.POST['tanggal']
		response['waktu'] = request.POST['waktu']
		response['tempat'] = request.POST['tempat']
		jdwl = Jadwals(nama = response['nama'], tipe = response['tipe'], tanggal = response['tanggal'], waktu = response['waktu'], tempat = response['tempat'])
		jdwl.save()
		form = FormJadwal()
		context = {
			'form' : form,
			'jwl' : jwl
		}
		return render(request, 'Jadwal.html', context)
	
	else:
		form = FormJadwal()
		context = {
			'form' : form,
			'jwl' : jwl
		}
		return render(request, 'Jadwal.html', context)
