from django.db import models
from django.utils import timezone

class Jadwals(models.Model):
	nama = models.CharField(max_length = 30)
	tipe = models.CharField(max_length = 30)
	tanggal = models.DateField(max_length = 30, default = timezone.now)
	waktu = models.TimeField(max_length = 30, default = timezone.now)
	tempat = models.CharField(max_length = 30)

