from django import forms
from .models import Jadwals

class FormJadwal(forms.Form):
	attrsText = {
		'class' : 'form-control'
	}
	
	attrsDate = {
		'class' : 'form-control',
		'type' : 'date'
	}
	
	attrsTime = {
		'class' : 'form-control',
		'type' : 'time'
	}
	nama = forms.CharField(label = 'Nama Kegiatan', max_length = 30, widget = forms.TextInput(attrs = attrsText), required = True)
	tipe = forms.CharField(label = 'Tipe Kegiatan', max_length = 30, widget = forms.TextInput(attrs = attrsText), required = True)
	tanggal = forms.DateField(label = 'Tanggal', widget = forms.TextInput(attrs = attrsDate), required = True)
	waktu = forms.TimeField(label = 'Waktu', widget = forms.TextInput(attrs = attrsTime), required = True)
	tempat = forms.CharField(label = 'Lokasi', max_length = 30, widget = forms.TextInput(attrs = attrsText), required = True)
