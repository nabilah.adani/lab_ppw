from django.urls import path
from .views import *

urlpatterns = [
    path('', pembuka, name='pembuka'),
    path('tentangsaya/', tentangsaya, name='tentangsaya'),
    path('selengkapnya/', selengkapnya, name='selengkapnya'),
    path('registrasi/', registrasi, name='registrasi'),
    path('buatjadwal/', buatjadwal, name='buatjadwal'),
]
