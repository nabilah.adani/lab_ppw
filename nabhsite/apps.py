from django.apps import AppConfig


class NabhsiteConfig(AppConfig):
    name = 'nabhsite'
